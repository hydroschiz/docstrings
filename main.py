"""
    Программа принимает на вход верхнюю границу диапазона
    и последовательно выводит счастливые простые числа,
    находящиеся в этом диапазоне,
    используя для этого функции определения того,
    является ли данное число простым и счастливым.
    Для демонстрации примеров работы функции определения
    счастливых чисел используются как случайные целые числа,
    генерируемые функцией randint из модуля random, так и заранее заготовленные примеры.
"""
from random import randint


def isprime(number: int) -> bool:
    """
    Возвращает логическое значение - результат определения того,
    является ли введенное число простым

        Параметры:
            number (int): целое проверяемое число

        Возвращаемое значение:
            True или False

        Генерируемые исключения:
            TypeError

        Примеры работы:
            isprime(17) -> True
            isprime(13) -> True
            isprime(27) -> False
            isprime(30) -> False
            isprime(2) -> True
    """
    if number in (0, 1):
        return False
    if number == 2:
        return True
    divisor = 2
    while divisor <= number ** 0.5:
        if number % divisor == 0:
            return False
        divisor += 1
    return True


def ishappy(number: int) -> bool:
    """
    Возвращает логическое значение - результат определения того,
    является ли введенное число счастливым

        Параметры:
            number (int): целое проверяемое число

        Возвращаемое значение:
            True или False

        Генерируемые исключения:
            TypeError
        
        Примеры работы:
            ishappy(7) -> True
            ishappy(44) -> True
            ishappy(130) -> True
            ishappy(9) -> False
            ishappy(121) -> False
    """
    for k in range(1000):
        separated_number = list(str(number))
        number = 0
        for item in range(len(separated_number)):
            number += int(separated_number[item]) ** 2
        if number == 1:
            return True
    return False


def example():
    """
    Выводит примеры работы функции ishappy,
    использует функцию randint для
    генерации случайных целых чисел
    """
    print('Для числа 7 результат работы - ', ishappy(7))
    print('Для числа 536 результат работы - ', ishappy(536))
    print('Для числа 940 результат работы - ', ishappy(940))
    for i in range(3):
        number = randint(1, 500)
        print(f'Для числа {number} результат - {ishappy(number)}')


def action(limit: int):
    """
    Принимает целое число limit и выводит
    простые счастливые числа в диапазоне от 0 до limit

        Параметры:
            limit (int): целое число, верхняя граница диапазона
    """
    print(f'Список счастливых простых чисел в диапазоне от 0 до {limit}:')
    for i in range(limit + 1):
        if ishappy(i) and isprime(i):
            print(i)


def main():
    choice = None
    while choice != '0':

        print('Выберите действие:')
        print('1 - Ввести число N и получить список счастливых простых чисел в диапазоне от 0 до N')
        print('2 - Показать примеры работы функции определения счастливых чисел')
        print('0 - Выйти')

        choice = input()

        if choice == '1':
            print('Введите верхнюю границу диапазона')

            limit = input()

            try:
                limit = int(limit)
            except ValueError:
                print('Введенное значение не подходит')
                continue

            if limit < 0:
                print('Граница не может быть отрицательным числом')
                continue

            action(limit)
            continue

        if choice == '2':
            example()
            continue

        if choice not in ('0', '1', '2'):
            print('Введено неверное значение')
            continue


if __name__ == '__main__':
    main()
